/* Development utility 'init-theme.js' is a snippet to prevent flickering when
 * loading themes on pages with long loading times. Add the minified inline
 * script in a '<script>' to your '<head>' before other scripts. Read Web
 * Assets wiki for details.
 */
(function() {
  function initTheme() {
    // Create variable root
    const rootElement = document.documentElement;

    // Init variable 'themeValue'
    let themeValue;

    // Create function 'getThemeValue'
    function getThemeValue() {
      // Get 'themeValue'
      themeValue = localStorage.getItem('dataTheme');

      return themeValue;
    }

    // Create function 'setTheme'
    function setTheme(data) {
      getThemeValue();

      rootElement.setAttribute('data-theme', data);
    }

    // Create function 'init'
    function init() {
      getThemeValue();

      if (themeValue) {
        // If 'themeValue' is set, set theme active
        setTheme(themeValue);
      }
    }

    init();
  }

  initTheme();
})();
