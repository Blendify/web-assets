(function() {
  function toggleTheme() {
    // Create variables root
    const rootElement = document.documentElement;
    const rootElementTheme = rootElement.getAttribute('data-theme');

    // Create variables theme btn
    const toggleThemeBtn = document.querySelector('.js-toggle-theme-btn');
    const toggleThemeBtnIcon = document.querySelector('.js-toggle-theme-btn-icon');

    if (!toggleThemeBtn) {
      // Stop execution if 'toggleThemeBtn' not exists'
      return;
    }

    // Init variable 'themeValue'
    let themeValue;

    function resetToggleThemeBtnIcon() {
      const classNames = toggleThemeBtnIcon.classList;

      classNames.forEach(function(className) {
        if (className.startsWith('i-')) {
          toggleThemeBtnIcon.classList.remove(className);
        }
      });
    }

    // Create function 'changeToggleThemeBtnIcon' to manage 'toggleThemeBtn' state
    function changeToggleThemeBtnIcon(data) {
      resetToggleThemeBtnIcon();

      if (data == 'dark') {
        // If theme is 'dark', set toggle theme btn moon
        toggleThemeBtnIcon.classList.add('i-moon');
      } else if (data == 'light') {
        // If theme is 'light', set toggle theme btn moon
        toggleThemeBtnIcon.classList.add('i-sun');
      } else {
        // If theme is 'system', set toggle theme btn adjust
        toggleThemeBtnIcon.classList.add('i-adjust')
      }
    }

    // Create function 'getThemeValue'
    function getThemeValue() {
      // Get 'themeValue'
      themeValue = localStorage.getItem('dataTheme');

      return themeValue;
    }

    // Create function 'setTheme'
    function setTheme(data) {
      localStorage.setItem('dataTheme', data);

      getThemeValue();

      if (themeValue !== 'system') {
        // Set root attribute data-theme if theme active is not 'system'
        rootElement.setAttribute('data-theme', data);
      } else {
        // Clear root attribute data-theme if theme active is 'system'
        rootElement.removeAttribute('data-theme');
      }
    }

    // Create function 'setTheme'
    function init() {
      getThemeValue();

      if (themeValue) {
        // If 'themeValue' is set, set theme active
        setTheme(themeValue);

        changeToggleThemeBtnIcon(themeValue);
      } else {
        if (rootElementTheme) {
          // Init 'rootElementTheme' on load
          setTheme(rootElementTheme);

          changeToggleThemeBtnIcon(rootElementTheme);
        }
      }

      // Create event toggle theme btn
      toggleThemeBtn.addEventListener('click', function() {
        getThemeValue();

        if (themeValue == 'dark') {
          setTheme('system');

          // Change to next swap state
          changeToggleThemeBtnIcon('system');
        } else if (themeValue == 'light') {
          setTheme('dark')

          changeToggleThemeBtnIcon('dark');
        } else {
          setTheme('light');

          changeToggleThemeBtnIcon('light');
        }
      });
    }

    init();
  }

  // Create function init
  function init() {
    toggleTheme();
  }

  // Call function init when document has been loaded
  if (document.readyState == "loading") {
    // If document is loading, wait before initializing
    document.addEventListener("DOMContentLoaded", function() {
      init();
    });
  } else {
    // If document has been loaded, intialize
    init();
  }
}());
